﻿using GriddlerSolver_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GriddlerSolver_BL
{
    public class DelimitedTextFromFileFactory:IGridCreatorFactory
    {
        public IGridCreator getGridCreator()
        {
            ITextReader textReader = new ResourceTextReader();
            return new DelimitedTextGridCreator(textReader);
        }
    }
}
