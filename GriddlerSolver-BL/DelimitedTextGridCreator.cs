﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GriddlerSolver_Utils.models;
using GriddlerSolver_DAL;

namespace GriddlerSolver_BL
{
    public class DelimitedTextGridCreator : IGridCreator
    {
        private ITextReader textReader;
        public DelimitedTextGridCreator(ITextReader textReader)
        {
            this.textReader = textReader;
        }
        public Grid CreateGrid()
        {
            Grid grid = new Grid();

            string gridText = textReader.ReadText();
            string[] gridTextLines = gridText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            string columnsLine = gridTextLines[0];
            string rowsLine = gridTextLines[1];
            
            List<List<int>> columnValues = getValuesFromLine(columnsLine);
            List<List<int>> rowValues = getValuesFromLine(rowsLine);

            grid.rowValues = rowValues;
            grid.columnValues = columnValues;

            InitializeGrid(grid);

            return grid;
        }

        private static void InitializeGrid(Grid grid)
        {
            grid.blocks = new Block[grid.columnValues.Count, grid.rowValues.Count];

            for (int i = 0; i < grid.blocks.GetLength(0); i++)
            {
                for (int j = 0; j < grid.blocks.GetLength(1); j++)
                {
                    grid.blocks[i, j] = new Block();
                    grid.blocks[i, j].state = BlockState.unknown;
                }
            }
        }

        private static List<List<int>> getValuesFromLine(string line)
        {
            string[] lineSplit = line.Split(new char[1] { ',' });
            List<List<int>> valuesArray = new List<List<int>>();

            for (int i = 0; i < lineSplit.Length; i++)
            {
                List<string> stringValues = lineSplit[i].Split(new char[1] { ' ' }).ToList();
                List<int> values = stringValues.Select(s => int.Parse(s)).ToList();
                valuesArray.Add(values);
            }
            return valuesArray;
        }
    }
}
