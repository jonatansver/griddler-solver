﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GriddlerSolver_Utils.models;

namespace GriddlerSolver_BL
{
    public interface IGridCreator
    {
        Grid CreateGrid();
    }
}
