﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GriddlerSolver_DAL
{
    public class FileReader : ITextReader
    {
        private string filePath;
        public FileReader(string filePath)
        {
            this.filePath = filePath;
        }
        public string ReadText()
        {
            try
            {
                return System.IO.File.ReadAllText(this.filePath);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
