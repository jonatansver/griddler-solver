﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GriddlerSolver_DAL
{
    public interface ITextReader
    {
        string ReadText();
    }
}
