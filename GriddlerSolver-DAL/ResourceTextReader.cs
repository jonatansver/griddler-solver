﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GriddlerSolver_DAL
{
    public class ResourceTextReader:ITextReader
    {
        public string ReadText()
        {
            return GriddlerSolver_Utils.Properties.Resources.GriddlerGrid;
        }
    }
}
