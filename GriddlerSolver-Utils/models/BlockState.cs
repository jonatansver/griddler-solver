﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GriddlerSolver_Utils.models
{
    public enum BlockState
    {
        filled,
        blank,
        unknown
    }
}
