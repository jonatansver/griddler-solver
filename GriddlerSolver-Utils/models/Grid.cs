﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GriddlerSolver_Utils.models
{
    public class Grid
    {
        public List<List<int>> rowValues { get; set; }
        public List<List<int>> columnValues { get; set; }
        public Block[,] blocks { get; set; }
    }
}
