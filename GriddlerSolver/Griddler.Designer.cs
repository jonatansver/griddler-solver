﻿namespace GriddlerSolver
{
    partial class Griddler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.griddlerTableWrapper = new System.Windows.Forms.Panel();
            this.mainGriddlerTable = new System.Windows.Forms.TableLayoutPanel();
            this.colmnValuesTable = new System.Windows.Forms.TableLayoutPanel();
            this.rowValuesTable = new System.Windows.Forms.TableLayoutPanel();
            this.gridTable = new System.Windows.Forms.TableLayoutPanel();
            this.griddlerTableWrapper.SuspendLayout();
            this.mainGriddlerTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // griddlerTableWrapper
            // 
            this.griddlerTableWrapper.Controls.Add(this.mainGriddlerTable);
            this.griddlerTableWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griddlerTableWrapper.Location = new System.Drawing.Point(0, 0);
            this.griddlerTableWrapper.Name = "griddlerTableWrapper";
            this.griddlerTableWrapper.Padding = new System.Windows.Forms.Padding(0, 40, 0, 0);
            this.griddlerTableWrapper.Size = new System.Drawing.Size(700, 740);
            this.griddlerTableWrapper.TabIndex = 0;
            // 
            // mainGriddlerTable
            // 
            this.mainGriddlerTable.AutoSize = true;
            this.mainGriddlerTable.ColumnCount = 2;
            this.mainGriddlerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.mainGriddlerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.mainGriddlerTable.Controls.Add(this.colmnValuesTable, 1, 0);
            this.mainGriddlerTable.Controls.Add(this.rowValuesTable, 0, 1);
            this.mainGriddlerTable.Controls.Add(this.gridTable, 1, 1);
            this.mainGriddlerTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGriddlerTable.Location = new System.Drawing.Point(0, 40);
            this.mainGriddlerTable.Name = "mainGriddlerTable";
            this.mainGriddlerTable.RowCount = 2;
            this.mainGriddlerTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.mainGriddlerTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.mainGriddlerTable.Size = new System.Drawing.Size(700, 700);
            this.mainGriddlerTable.TabIndex = 0;
            // 
            // colmnValuesTable
            // 
            this.colmnValuesTable.ColumnCount = 2;
            this.colmnValuesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.colmnValuesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.colmnValuesTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colmnValuesTable.Location = new System.Drawing.Point(143, 3);
            this.colmnValuesTable.Name = "colmnValuesTable";
            this.colmnValuesTable.RowCount = 2;
            this.colmnValuesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.colmnValuesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.colmnValuesTable.Size = new System.Drawing.Size(554, 134);
            this.colmnValuesTable.TabIndex = 0;
            // 
            // rowValuesTable
            // 
            this.rowValuesTable.ColumnCount = 2;
            this.rowValuesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.rowValuesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.rowValuesTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rowValuesTable.Location = new System.Drawing.Point(3, 143);
            this.rowValuesTable.Name = "rowValuesTable";
            this.rowValuesTable.RowCount = 2;
            this.rowValuesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.rowValuesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.rowValuesTable.Size = new System.Drawing.Size(134, 554);
            this.rowValuesTable.TabIndex = 1;
            // 
            // gridTable
            // 
            this.gridTable.ColumnCount = 2;
            this.gridTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gridTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gridTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTable.Location = new System.Drawing.Point(143, 143);
            this.gridTable.Name = "gridTable";
            this.gridTable.RowCount = 2;
            this.gridTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gridTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gridTable.Size = new System.Drawing.Size(554, 554);
            this.gridTable.TabIndex = 2;
            // 
            // Griddler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(700, 740);
            this.Controls.Add(this.griddlerTableWrapper);
            this.Name = "Griddler";
            this.Text = "Form1";
            this.griddlerTableWrapper.ResumeLayout(false);
            this.griddlerTableWrapper.PerformLayout();
            this.mainGriddlerTable.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel griddlerTableWrapper;
        private System.Windows.Forms.TableLayoutPanel mainGriddlerTable;
        private System.Windows.Forms.TableLayoutPanel colmnValuesTable;
        private System.Windows.Forms.TableLayoutPanel rowValuesTable;
        private System.Windows.Forms.TableLayoutPanel gridTable;
    }
}

