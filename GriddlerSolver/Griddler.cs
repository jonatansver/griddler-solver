﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GriddlerSolver_BL;

namespace GriddlerSolver
{
    public partial class Griddler : Form
    {
        public Griddler(IGridCreatorFactory gridCreatorFactory)
        {
            InitializeComponent();
            InitializeGrid(gridCreatorFactory.getGridCreator().CreateGrid());
        }

        private void InitializeGrid(GriddlerSolver_Utils.models.Grid grid)
        {
            InitializeMainGrid(grid);
            InitializeValuesPannels(grid);
        }

        private void InitializeMainGrid(GriddlerSolver_Utils.models.Grid grid)
        {
            gridTable.ColumnCount = grid.blocks.GetLength(0);
            gridTable.RowCount = grid.blocks.GetLength(1);
            gridTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;
            InitializeTableLayoutPannel(gridTable.ColumnCount, gridTable.RowCount, gridTable);

        }

        private void InitializeValuesPannels(GriddlerSolver_Utils.models.Grid grid)
        {
            int columnValuesColumnCount = grid.blocks.GetLength(0);
            int columnValuesRowCount = grid.columnValues.OrderByDescending(colVal => colVal.Count).First().Count;
            int rowValuesColumnCount = grid.rowValues.OrderByDescending(rowVal => rowVal.Count).First().Count;
            int rowValuesRowCount = grid.blocks.GetLength(1);
            InitializeTableLayoutPannel(columnValuesColumnCount, columnValuesRowCount, colmnValuesTable);
            InitializeTableLayoutPannel(rowValuesColumnCount, rowValuesRowCount, rowValuesTable);
            List<List<int>> list = grid.rowValues;
            List<List<int>> flippedList = Enumerable.Range(0, list[0].Count)
                .Select(x => Enumerable.Range(0, list.Count)
                    .Select(y => list[y][x]).ToList())
                    .ToList();
            InitializeValuesPannelValues(colmnValuesTable, grid.columnValues);
            InitializeValuesPannelValues(rowValuesTable, flippedList);
        }

        private List<List<T>> Flip2DList(List<List<T> list){

        }
        private static void InitializeValuesPannelValues(TableLayoutPanel tableLayoutPannel, List<List<int>> values)
        {
            for (int i = 0; i < tableLayoutPannel.ColumnCount; i++)
            {
                for (int j = 0; j < tableLayoutPannel.RowCount; j++)
                {
                    string currentCellValue = "";
                    if (values[i].Count > j)
                    {
                        currentCellValue = values[i][j].ToString();
                        Label valueLabel = new Label();
                        valueLabel.Text = currentCellValue.ToString();
                        valueLabel.Dock = DockStyle.Fill;
                        valueLabel.TextAlign = ContentAlignment.MiddleCenter;
                        tableLayoutPannel.Controls.Add(valueLabel,
                        tableLayoutPannel.ColumnCount - i - 1, tableLayoutPannel.RowCount - j - 1);
                    }
                }
            }
        }

        private static void InitializeTableLayoutPannel(int valuesColumnCount, int valuesRowCount, TableLayoutPanel tableLayoutPannel)
        {
            tableLayoutPannel.ColumnCount = valuesColumnCount;
            tableLayoutPannel.RowCount = valuesRowCount;
            tableLayoutPannel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;
            tableLayoutPannel.ColumnStyles.Clear();
            for (int i = 0; i < valuesColumnCount; i++)
            {
                tableLayoutPannel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            }
            tableLayoutPannel.RowStyles.Clear();
            for (int i = 0; i < valuesRowCount; i++)
            {
                tableLayoutPannel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            }
        }
    }
}
